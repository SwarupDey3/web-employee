FROM openjdk:8-jre-alpine

WORKDIR /app

COPY /target/*.jar /app/employee.jar

CMD ["java", "-jar", "employee.jar"]